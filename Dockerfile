FROM node:19-alpine3.16

WORKDIR /SIMPLE-REACTJS-APP

COPY . .

RUN npm install -g create-react-app

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]

